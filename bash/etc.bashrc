#
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias vi='vim'
case "$TERM" in
	xterm-color|*-256color) color_prompt=yes;;
esac

export PATH="./:$PATH"
export PS1="\e[0;31m\$?\e[0;37m::\e[0;36m\u\e[0;37m@\h [\e[1;31m\w\e[0;37m]\n$ "
