set number
map <C-l> <C-w><C-w>
set history=500

syntax on

filetype plugin on
filetype indent on

set autoread

let mapleader = ','

command Save :mks sess.vim!|:wqa

set so=7

set tabstop=4
set shiftwidth=4
set expandtab
 
let $LANG='en'
set ruler
set cmdheight=2
set formatoptions-=cro


set hid
set ignorecase
set smartcase
set hlsearch
set showmatch

set mouse=a
set noswapfile
set smarttab
set ai
set si
set wrap

" MAPPINGS "

map fix gg=G
map ss :setlocal spell!<cr>
map tt :tabnext<cr>
map list :! ll<cr>
map save :wqa<cr>
map dt :put =strftime('%a %b. %d, 20%y')<cr>

let @c = ':put = "#include <stdio.h>\n#include <stdlib.h>\n\nint main(){\n\n\n\treturn0\n}'

map <C-l> <C-w><C-w>

inoremap <S-UP> <C-n>
autocmd vimenter * wincmd p

let g:syntastic_python_checkers=['flake8']

map plugI :PlugInstall<cr>

syntax on
colorscheme challenger_deep
set t_Co=256

call plug#begin('~/.vim/plugged')
    Plug 'scrooloose/nerdcommenter'
    Plug 'bling/vim-airline'
	Plug 'scrooloose/syntastic'
	Plug 'majutsushi/tagbar'
    Plug 'ctrlpvim/ctrlp.vim'
	Plug 'terryma/vim-multiple-cursors'
    Plug 'vim-python/python-syntax'    
call plug#end()
