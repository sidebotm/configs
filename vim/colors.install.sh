#! /bin/bash

mkdir -p ~/.vim/colors

git clone https://github.com/levelone/tequila-sunrise.vim
cd tequila-sunrise.vim/colors
cp *.vim ~/.vim/colors/.
cd ../../
rm -rf tequila-sunrise.vim



git clone https://github.com/challenger-deep-theme/vim challenger
cd challenger/colors
cp *.vim ~/.vim/colors/.
cd ../../
rm -rf challenger
